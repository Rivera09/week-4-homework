export const HOME_PAGE = "HOME";
export const GAMES_LIST = "GAMES LIST";
export const GAME_DETAILS = "GAME DETAILS";

const BASE_URL = "http://localhost:3004/";

export const GAMES_URL = BASE_URL + "games?_page=:page&_limit=5";
export const CATEGORIES_URL = BASE_URL + "categories";
export const FEATURED_GAMES = BASE_URL + "games?featured=true&_page=1&_limit=7";
export const COMMENTS_URL = BASE_URL + "comments?gameId=:gameId";
