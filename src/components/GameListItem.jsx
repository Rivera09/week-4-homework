import React from "react";

import { getRealPrice } from "../helpers";

const GameListItem = ({
  gameDetails: { name, price, discount, logo, studio },
  onClick,
  category,
}) => {
  return (
    <div className="games-list-item" onClick={() => onClick.call()}>
      <div className="list-item-bg">
        <div className="tint-color"></div>
        <div className="secondary-color"></div>
      </div>
      <div className="list-item-info">
        <div>
          <img src={logo} alt="game logo" />
          <div className="list-item-info-name">
            <h2>
              {name} - {studio}
            </h2>
            <p>{category}</p>
          </div>
        </div>
        <div className="list-item-info-price">
          <p className="prev-price">{discount ? price + "$" : null}</p>
          <p className="price">{getRealPrice(price, discount)}</p>
        </div>
      </div>
    </div>
  );
};

export default GameListItem;
