import React, { useContext, useState } from "react";

import logo from "../img/logo.png";
import { HOME_PAGE, GAMES_LIST } from "../consts";
import { RouterContext } from "../routerContext";

const Navbar = () => {
  const { setCurrentPage } = useContext(RouterContext);
  const [active, setActive] = useState(false);
  return (
    <nav className={`main-nav ${active ? "active" : ""}`}>
      <img src={logo} alt="logo" onClick={() => setCurrentPage(HOME_PAGE)} />
      <ul className={active ? "active" : ""}>
        <li onClick={() => setCurrentPage(HOME_PAGE)}>Home</li>
        <li onClick={() => setCurrentPage(GAMES_LIST)}>Games</li>
      </ul>
      <div
        className={`burger ${active ? "active" : ""}`}
        onClick={() => setActive((prevValue) => !prevValue)}
      >
        <div className="line1"></div>
        <div className="line2"></div>
        <div className="line3"></div>
      </div>
    </nav>
  );
};

export default Navbar;
