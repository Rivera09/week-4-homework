import React from "react";

import { getRealPrice } from "../helpers";

const GameCard = ({
  gameData: { description, discount, price, name, image },
  onClick,
}) => {
  return (
    <div className="game-card" onClick={() => onClick.call()}>
      <div className="game-info">
        <h2 className="game-title">{name}</h2>
        <p className="game-desc">{description}</p>
      </div>
      <div className="game-price">
        <h3>PRICE: </h3>
        <h3>{getRealPrice(price, discount)}</h3>
      </div>
      <img src={image} alt={name} className="bg-image" />
    </div>
  );
};

export default GameCard;
