import React, { useContext, useEffect, useState } from "react";

import { GameCard, Header } from "../components";
import { FEATURED_GAMES, GAME_DETAILS } from "../consts";
import { RouterContext } from "../routerContext";

const Home = () => {
  const { setCurrentPage, apiConnection, setGameData } = useContext(
    RouterContext
  );
  const [weekGame, setWeekGame] = useState({});
  const [gamesList, setGamesList] = useState([]);
  useEffect(() => {
    const fetchFeaturedGames = async () => {
      const { resBody, error } = await apiConnection.getData(FEATURED_GAMES);
      if (error) return;
      const weekGameIndex = resBody.findIndex((game) => game.weekGame);
      setWeekGame(resBody.splice(weekGameIndex, 1).pop());
      setGamesList(resBody);
    };
    fetchFeaturedGames();
  }, [apiConnection]);

  const handleOnClick = (gameData) => {
    setCurrentPage(GAME_DETAILS);
    setGameData(gameData);
  };
  return (
    <>
      <Header />
      <div className="featured-games">
        <h3>This week's game </h3>
        <div className="week-game">
          <GameCard
            gameData={weekGame}
            onClick={() => handleOnClick(weekGame)}
          />
        </div>
        <h3>Featured games</h3>
        <div className="games-carousel">
          {gamesList.map((gameItem) => (
            <GameCard
              gameData={gameItem}
              key={gameItem.id}
              onClick={() => handleOnClick(gameItem)}
            />
          ))}
        </div>
      </div>
    </>
  );
};

export default Home;
