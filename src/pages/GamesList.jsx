import React, { useState, useEffect, useContext } from "react";

import { Header, GameListItem } from "../components";
import { RouterContext } from "../routerContext";
import { GAMES_URL, GAME_DETAILS } from "../consts";

const GamesList = () => {
  const { apiConnection, setGameData, setCurrentPage, categories } = useContext(
    RouterContext
  );
  const [gamesList, setGamesList] = useState([]);
  const [currentPagination, setCurrentPagination] = useState(1);
  useEffect(() => {
    const fetchGames = async () => {
      const { resBody, error } = await apiConnection.getData(
        GAMES_URL.replace(":page", currentPagination)
      );
      if (error) return console.log("Error");
      setGamesList((prevGames) => [...prevGames, ...resBody]);
    };
    fetchGames();
  }, [currentPagination, apiConnection]);
  return (
    <>
      <Header title="List of games" />
      <div className="games-list-container">
        <div className="games-list">
          {gamesList.map((gameItem) => (
            <GameListItem
              category={categories[gameItem.category - 1].name}
              gameDetails={gameItem}
              key={gameItem.id}
              onClick={() => {
                setCurrentPage(GAME_DETAILS);
                setGameData(gameItem);
              }}
            />
          ))}
        </div>
        <div
          className="games-list-item"
          onClick={() => setCurrentPagination((prevPage) => prevPage + 1)}
        >
          <div className="list-item-bg">
            <div className="tint-color"></div>
            <div className="secondary-color"></div>
          </div>
          <button>load more</button>
        </div>
      </div>
    </>
  );
};

export default GamesList;
