import React, { useContext, useEffect, useState } from "react";

import { Navbar, Comment } from "../components";
import { getRealPrice } from "../helpers";
import { RouterContext } from "../routerContext";
import { COMMENTS_URL } from "../consts";

const GameDetails = ({
  gameData: { image, name, description, discount, price, studio, id },
  category,
}) => {
  const { apiConnection } = useContext(RouterContext);
  const [gameComments, setGameComment] = useState([]);

  useEffect(() => {
    const fetchComments = async () => {
      const { resBody } = await apiConnection.getData(
        COMMENTS_URL.replace(":gameId", id)
      );

      setGameComment(resBody);
    };
    fetchComments();
  }, [apiConnection, id]);
  // console.log(gameComments);
  return (
    <div className="game-details-container">
      <Navbar />
      <div className="hero">
        <img src={image} alt="gta v" />
      </div>
      <div className="game-details">
        <div>
          <h2>
            {name} - {studio}
          </h2>
          <p>{category}</p>
        </div>
        <div className="prices-container">
          <h4>PRICE:</h4>
          <div>
            <p className="prev-price">{discount ? price + "$" : null}</p>
            <p className="price">{getRealPrice(price, discount)}</p>
          </div>
        </div>
        <p className="game-details-desc">{description}</p>
        <div className="get-btn-container">
          <div className="list-item-bg">
            <div className="tint-color"></div>
            <div className="secondary-color"></div>
          </div>
          <button>Get</button>
        </div>
      </div>
      <div className="comment-section">
        <h2>Comments</h2>
        {gameComments.map(({ id, userName, comment }) => (
          <Comment userName={userName} comment={comment} key={id} />
        ))}
      </div>
    </div>
  );
};

export default GameDetails;
