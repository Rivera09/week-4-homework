import React, { useState, useEffect } from "react";

import { RouterProvider } from "./routerContext";
import { HOME_PAGE, GAMES_LIST, CATEGORIES_URL } from "./consts";
import { Home, GameDetails, GamesList } from "./pages";
import { Footer } from "./components";
import Connection from "./apiConection";

import "./sass/style.scss";

const apiConnection = new Connection();

function App() {
  const [currentPage, setCurrentPage] = useState(HOME_PAGE);
  const [gameData, setGameData] = useState({});
  const [categories, setCategories] = useState([]);
  useEffect(() => {
    const fetchCategories = async () => {
      const { resBody, error } = await apiConnection.getData(CATEGORIES_URL);
      if (error) return;
      setCategories(resBody);
    };
    fetchCategories();
  }, []);
  return (
    <>
      <RouterProvider
        value={{ setCurrentPage, setGameData, apiConnection, categories }}
      >
        {currentPage === HOME_PAGE ? (
          <Home />
        ) : currentPage === GAMES_LIST ? (
          <GamesList />
        ) : (
          <GameDetails
            gameData={gameData}
            category={categories[gameData.category - 1].name}
          />
        )}
      </RouterProvider>
      <Footer />
    </>
  );
}

export default App;
