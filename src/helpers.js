export const getRealPrice = (price, discount) =>
  discount ? (price - (price * discount) / 100) + "$" : price ? price + "$" : "FREE";
